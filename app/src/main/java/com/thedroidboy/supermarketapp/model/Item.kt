package com.thedroidboy.supermarketapp.model

data class Item(val name: String, val weight: String, val bagColor: String)