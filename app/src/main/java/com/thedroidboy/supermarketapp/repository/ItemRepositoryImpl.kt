package com.thedroidboy.supermarketapp.repository

import androidx.lifecycle.LiveData
import com.thedroidboy.supermarketapp.model.Item
import com.thedroidboy.supermarketapp.network.SuperItemService

class ItemRepositoryImpl(private val superItemService: SuperItemService): ItemRepository {

    override fun registerToStream(): LiveData<Item> {
        return superItemService.openSocket()
    }

    override fun unRegisterToStream() {
        superItemService.closeSocket()
    }

}