package com.thedroidboy.supermarketapp.repository

import androidx.lifecycle.LiveData
import com.thedroidboy.supermarketapp.model.Item

interface ItemRepository {

    /**
     * register to stream of data. It can be fetch from everywhere
     */
    fun registerToStream() : LiveData<Item>

    fun unRegisterToStream()
}