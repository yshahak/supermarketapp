package com.thedroidboy.supermarketapp.network

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.thedroidboy.supermarketapp.model.Item
import okhttp3.*

class SuperItemServiceImpl(private val client: OkHttpClient, private val gson: Gson) : SuperItemService,
    WebSocketListener() {

    private val itemLiveData = MutableLiveData<Item>()
    private lateinit var socket: WebSocket

    override fun openSocket(): LiveData<Item> {
        start()
        return itemLiveData
    }

    override fun closeSocket() {
        socket.cancel()
    }

    private fun start() {
        val request = Request.Builder().url(SOCKET_URL).build()
        socket = client.newWebSocket(request, this)
        // Trigger shutdown of the dispatcher's executor so this process can exit cleanly.
        client.dispatcher().executorService().shutdown()
    }

    override fun onOpen(webSocket: WebSocket, response: Response) {
        super.onOpen(webSocket, response)
        Log.d(TAG, "socket opened")
    }

    override fun onMessage(webSocket: WebSocket, message: String) {
        super.onMessage(webSocket, message)
        val item = gson.fromJson(message, Item::class.java)
        Log.d(TAG, item.toString())
        itemLiveData.postValue(item)
    }

    companion object {
        const val TAG = "SuperItemServiceImpl"
        const val SOCKET_URL = "ws://superdo-groceries.herokuapp.com/receive"
    }
}

