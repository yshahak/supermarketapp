package com.thedroidboy.supermarketapp.network

import androidx.lifecycle.LiveData
import com.thedroidboy.supermarketapp.model.Item

interface SuperItemService {


    /**
     * open a socket to WebSocket
     */
    fun openSocket(): LiveData<Item>

    fun closeSocket()
}