package com.thedroidboy.supermarketapp.vm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.thedroidboy.supermarketapp.model.Item
import com.thedroidboy.supermarketapp.network.SuperItemServiceImpl
import com.thedroidboy.supermarketapp.repository.ItemRepository
import com.thedroidboy.supermarketapp.repository.ItemRepositoryImpl
import okhttp3.OkHttpClient

class ItemsViewModel : ViewModel(), Observer<Item> {

    private lateinit var repo: ItemRepository
    private val itemList = mutableListOf<Item>()
    private val itemListLiveData = MutableLiveData<List<Item>>()

    private lateinit var itemLiveData: LiveData<Item>
    private var isFetchingData = true

    fun getItemList(): LiveData<List<Item>>{
        val service = SuperItemServiceImpl(OkHttpClient(), Gson())
        repo = ItemRepositoryImpl(service)
        itemLiveData = repo.registerToStream()
        itemLiveData.observeForever(this)
        return itemListLiveData
    }

    fun onButtonPressed(){
        if (isFetchingData){
//            repo.unRegisterToStream()
            itemLiveData.removeObserver(this)
        } else {
//            repo.registerToStream()
            itemLiveData.observeForever(this)
        }
        isFetchingData = !isFetchingData
    }

    override fun onChanged(item: Item) {
        itemList.add(0, item)
        itemListLiveData.postValue(itemList)
    }

    override fun onCleared() {
        super.onCleared()
        itemLiveData.removeObserver(this)
        repo.unRegisterToStream()
    }
}
