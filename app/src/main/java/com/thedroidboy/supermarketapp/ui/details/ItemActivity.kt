package com.thedroidboy.supermarketapp.ui.details

import android.graphics.Color
import android.os.Bundle
import android.widget.FrameLayout
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import com.thedroidboy.supermarketapp.R

import kotlinx.android.synthetic.main.activity_item.*

class ItemActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item)
        val color = intent.getStringExtra("color")
        findViewById<FrameLayout>(R.id.root).setBackgroundColor(Color.parseColor(color))
    }

}
