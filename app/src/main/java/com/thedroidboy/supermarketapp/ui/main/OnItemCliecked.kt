package com.thedroidboy.supermarketapp.ui.main

import com.thedroidboy.supermarketapp.model.Item

interface OnItemClicked {

    fun onItemClicked(item: Item)
}