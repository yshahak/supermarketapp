package com.thedroidboy.supermarketapp.ui.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.thedroidboy.supermarketapp.R
import com.thedroidboy.supermarketapp.model.Item
import com.thedroidboy.supermarketapp.ui.details.ItemActivity
import com.thedroidboy.supermarketapp.vm.ItemsViewModel

class MainActivity : AppCompatActivity(),OnItemClicked {


    private lateinit var viewModel: ItemsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        val viewManager = LinearLayoutManager(this)
        val recyclerView = findViewById<RecyclerView>(R.id.recycler_view).apply {
            layoutManager = viewManager
        }
        viewModel = ViewModelProviders.of(this).get(ItemsViewModel::class.java)
        viewModel.getItemList().observe(this, Observer { itemList ->
            recyclerView.adapter?.apply {
                //state meant to save the right position of the recycler
                val state = recyclerView.layoutManager?.onSaveInstanceState()
                notifyItemInserted(0)
                recyclerView.layoutManager?.onRestoreInstanceState(state)
            } ?: let {
                //initialize the adapter
                recyclerView.adapter = ItemsRecyclerViewAdapter(itemList, this)
            }
        })
        val fab: View = findViewById(R.id.fab)
        fab.setOnClickListener {
            viewModel.onButtonPressed()
        }
    }

    override fun onItemClicked(item: Item) {
        Intent(this, ItemActivity::class.java).apply {
            this.putExtra("color", item.bagColor)
        }.run {
            startActivity(this)
        }
    }

}

