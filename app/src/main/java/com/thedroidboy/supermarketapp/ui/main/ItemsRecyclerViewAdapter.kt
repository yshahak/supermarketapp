package com.thedroidboy.supermarketapp.ui.main

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.thedroidboy.supermarketapp.R
import com.thedroidboy.supermarketapp.model.Item


class ItemsRecyclerViewAdapter(
    private val items: List<Item>, private val onItemClicked: OnItemClicked
) : RecyclerView.Adapter<ItemViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val root = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_row, parent, false) as ViewGroup
        root.setOnClickListener {
            onItemClicked.onItemClicked(root.tag as Item)
        }
        return ItemViewHolder(root)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = items[position]
        holder.icon.text = item.bagColor
        holder.icon.setTextColor(Color.parseColor(item.bagColor))
        holder.label.text = item.name
        holder.weight.text = item.weight
        holder.root.tag = item
    }

}

class ItemViewHolder(val root: ViewGroup) : RecyclerView.ViewHolder(root) {
    val icon = root.findViewById<TextView>(R.id.icon_ball)
    val label = root.findViewById<TextView>(R.id.label)
    val weight = root.findViewById<TextView>(R.id.weight)
}


